package org.docascode.ant;

import org.apache.tools.ant.BuildException;
import org.docascode.api.BuildCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.DocAsCodeRepositoryTestCase;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.office.CheckBoxProcessor;
import org.docascode.api.core.office.PropertiesProcessor;
import org.docascode.api.core.office.Utils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class UpdateTaskTest extends DocAsCodeRepositoryTestCase {
    @BeforeEach
    public void setUp(TestInfo testInfo) throws IOException, GitAPIException, DocAsCodeException {
        super.setUp(testInfo);
    }

    @Test
    void testUpdate() throws DocAsCodeException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("update");
                }})
                .call();
        File docx = new File(repository.getWorkTree(),"source/myDoc1.docx");
        Map<String,String> actualDocxProperties = new PropertiesProcessor().list(docx);
        Map<String,String> expectedDocxProperties = new HashMap<>();
        expectedDocxProperties.put("Title","myNewTitle1");
        expectedDocxProperties.put("version","1.0");
        expectedDocxProperties.put("myName","myValue");
        assertEquals(actualDocxProperties, expectedDocxProperties);

        File xlsx = new File(repository.getWorkTree(),"source/myDoc1-attach1.xlsx");
        Map<String,String> actualXlsxProperties = new PropertiesProcessor().list(xlsx);
        Map<String,String> expectedXlsxxProperties = new HashMap<>();
        expectedXlsxxProperties.put("Title","myNewTitle1-attach1");
        expectedXlsxxProperties.put("version","1.0");
        expectedXlsxxProperties.put("myName","myValue");
        assertEquals(actualXlsxProperties, expectedXlsxxProperties);

        File pptx = new File(repository.getWorkTree(),"source/myDoc1-attach2.pptx");
        Map<String,String> actualPptxProperties = new PropertiesProcessor().list(pptx);
        Map<String,String> expectedPptxProperties = new HashMap<>();
        expectedPptxProperties.put("Title","myNewTitle1-attach2");
        expectedPptxProperties.put("version","1.0");
        expectedPptxProperties.put("myName","myValue");
        assertEquals(actualPptxProperties, expectedPptxProperties);
    }

    @Test
    void testUpdateNestedFileSet() throws DocAsCodeException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("update:fileset");
                }})
                .call();
        File docx = new File(repository.getWorkTree(),"source/myDoc1.docx");
        Map<String,String> actualDocxProperties = new PropertiesProcessor().list(docx);
        Map<String,String> expectedDocxProperties = new HashMap<>();
        expectedDocxProperties.put("Title","myNewTitle");
        expectedDocxProperties.put("version","1.0");
        expectedDocxProperties.put("myName","myValue");
        assertEquals(actualDocxProperties, expectedDocxProperties);

        File xlsx = new File(repository.getWorkTree(),"source/myDoc1-attach1.xlsx");
        Map<String,String> actualXlsxProperties = new PropertiesProcessor().list(xlsx);
        Map<String,String> expectedXlsxxProperties = new HashMap<>();
        expectedXlsxxProperties.put("Title","myNewTitle");
        expectedXlsxxProperties.put("version","1.0");
        expectedXlsxxProperties.put("myName","myValue");
        assertEquals(actualXlsxProperties, expectedXlsxxProperties);

        File pptx = new File(repository.getWorkTree(),"source/myDoc1-attach2.pptx");
        Map<String,String> actualPptxProperties = new PropertiesProcessor().list(pptx);
        Map<String,String> expectedPptxProperties = new HashMap<>();
        expectedPptxProperties.put("Title","myNewTitle");
        expectedPptxProperties.put("version","1.0");
        expectedPptxProperties.put("myName","myValue");
        assertEquals(actualPptxProperties, expectedPptxProperties);
    }

    @Test
    void testInsertRows_NoParagraphFormat() throws DocAsCodeException, Docx4JException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("insertRows:noParagraphFormat");
                }})
                .call();
        File docx = new File(repository.getWorkTree(),"build/myDoc1.docx");
        WordprocessingMLPackage openPackage=WordprocessingMLPackage.load(docx);
        List<Tbl> listTables = Utils.getTargetElements(openPackage.getMainDocumentPart(), Tbl.class);
        assertEquals(2,listTables.size());
        List<Tr> listRows = Utils.getTargetElements(listTables.get(0),Tr.class);
        assertEquals(2,listRows.size());
        List<Tc> listCells = Utils.getTargetElements(listRows.get(1),Tc.class);
        for ( Tc tc : listCells){
            List<P> listP = Utils.getTargetElements(tc,P.class);
            assertEquals(1,listP.size());
            assertNull(listP.get(0).getPPr());
        }
    }

    @Test
    void testInsertRows_NestedParagraphFormat() throws DocAsCodeException, Docx4JException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("insertRows:nestedParagraphFormat");
                }})
                .call();
        File docx = new File(repository.getWorkTree(),"build/myDoc1.docx");
        WordprocessingMLPackage openPackage=WordprocessingMLPackage.load(docx);
        List<Tbl> listTables = Utils.getTargetElements(openPackage.getMainDocumentPart(), Tbl.class);
        assertEquals(2,listTables.size());
        List<Tr> listRows = Utils.getTargetElements(listTables.get(0),Tr.class);
        assertEquals(3,listRows.size());
        List<Tc> listCells = Utils.getTargetElements(listRows.get(1),Tc.class);
        for ( Tc tc : listCells){
            assertEquals(JcEnumeration.RIGHT,Utils.getTargetElements(tc,P.class)
                    .get(0)
                    .getPPr()
                    .getJc()
                    .getVal());
        }

        listCells = Utils.getTargetElements(listRows.get(2),Tc.class);
        assertEquals(JcEnumeration.LEFT,
                Utils.getTargetElements(listCells.get(0),P.class)
                        .get(0)
                        .getPPr()
                        .getJc()
                        .getVal());
        assertEquals(JcEnumeration.LEFT,
                Utils.getTargetElements(listCells.get(1),P.class)
                        .get(0)
                        .getPPr()
                        .getJc()
                        .getVal());
        assertEquals(JcEnumeration.CENTER,
                Utils.getTargetElements(listCells.get(2),P.class)
                        .get(0)
                        .getPPr()
                        .getJc()
                        .getVal());
        assertEquals(JcEnumeration.LEFT,
                Utils.getTargetElements(listCells.get(3),P.class)
                        .get(0)
                        .getPPr()
                        .getJc()
                        .getVal());
        assertEquals(JcEnumeration.LEFT,
                Utils.getTargetElements(listCells.get(4),P.class)
                        .get(0)
                        .getPPr()
                        .getJc()
                        .getVal());
    }

    @Test
    void testInsertRows_TooMushCells(){
        assertThrows(BuildException.class,
                () -> ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                        .build()
                        .addListener(this))
                        .setTargets(new ArrayList<String>(){{
                            add("insertRows:tooMushCells");
                        }})
                        .call());
    }

    @Test
    void test_CheckBoxSet() throws DocAsCodeException, Docx4JException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("checkbox:set");
                }})
                .call();
        File docx = new File(repository.getWorkTree(),"build/myDoc1.docx");
        WordprocessingMLPackage openPackage = WordprocessingMLPackage.load(docx);
        CTFFCheckBox checkBox = CheckBoxProcessor.findCheckBox(openPackage,"checkbox");
        assertNotNull(checkBox);
        BooleanDefaultTrue expected = new BooleanDefaultTrue();
        expected.setVal(true);
        assertEquals(expected.isVal(),checkBox.getChecked().isVal());
    }

    @Test
    void test_CheckboxNotFound(){
        assertThrows(BuildException.class,
                () -> ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                        .build()
                        .addListener(this))
                        .setTargets(new ArrayList<String>(){{
                            add("checkbox:checkboxNotFound");
                        }})
                        .call());
    }
}
