package org.docascode.ant;

import org.apache.tools.ant.BuildException;
import org.docascode.api.BuildCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.DocAsCodeRepositoryTestCase;
import org.docascode.api.core.errors.DocAsCodeException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DocAsCodeAntTaskTestCase extends DocAsCodeRepositoryTestCase {

    @BeforeEach
    public void setUp(TestInfo testInfo) throws IOException, GitAPIException, DocAsCodeException {
        super.setUp(testInfo);
    }

    @Test
    void testProcessNominal() throws DocAsCodeException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("process:nominal");
                }})
                .call();
        assertTrue(new File(repository.getWorkTree(),"build/id1-/from/source/myDoc1.docx/to/AAAA-MM-JJ_myDoc1-Title_v1.0.docx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id1-beamer/from/source/myDoc1-attach2.pptx/to/AAAA-MM-JJ__v1.0.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id1-matrix/from/source/myDoc1-attach1.xlsx/to/AAAA-MM-JJ_myDoc1-matrix-Title_v1.0.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id2-/from/source/myDoc2.docx/to/AAAA-MM-JJ_myDoc2-Title_v1.0.docx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id2-beamer/from/source/myDoc2-attach2.pptx/to/AAAA-MM-JJ__v1.0.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id2-matrix/from/source/myDoc2-attach1.xlsx/to/AAAA-MM-JJ_myDoc2-matrix-Title_v1.0.xlsx").exists());
    }

    @Test
    void testProcessWithMapper() throws DocAsCodeException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("process:mapper");
                }})
                .call();
        assertTrue(new File(repository.getWorkTree(),"build/id1-/from/source/myDoc1.docx/to/AAAA-MM-JJ_myDoc1-Title_v1.0.docx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id1-beamer/from/source/myDoc1-attach2.pptx/to/AAAA-MM-JJ__v1.0.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id1-matrix/from/source/myDoc1-attach1.xlsx/to/AAAA-MM-JJ_myDoc1-matrix-Title_v1.0.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id2-/from/source/myDoc2.docx/to/AAAA-MM-JJ_myDoc2-Title_v1.0.docx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id2-beamer/from/source/myDoc2-attach2.pptx/to/AAAA-MM-JJ__v1.0.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),"build/id2-matrix/from/source/myDoc2-attach1.xlsx/to/AAAA-MM-JJ_myDoc2-matrix-Title_v1.0.xlsx").exists());
    }

    @Test
    void testProcessWithTooMuchNestedMapper() {
        assertThrows(BuildException.class,
                () -> ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("process:tooMuchMapper");
                }})
                .call());
    }




}
