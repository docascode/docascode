package org.docascode.ant;

import org.apache.commons.io.FileUtils;
import org.docascode.api.BuildCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DeployTaskTest extends DocAsCodeAntTaskTestCase{
    @BeforeEach
    public void setUp(TestInfo testInfo) throws IOException, GitAPIException, DocAsCodeException {
        File local = new File(System.getProperty( "user.home" ),
                ".m2/repository/org/docascode/test");
        super.setUp(testInfo);
        if (local.exists()) {
            FileUtils.deleteDirectory(local);
        }
    }

    @Test
    void testEmbeddedDeploy() throws DocAsCodeException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                        .build()
                        .addListener(this))
                        .setTargets(new ArrayList<String>(){{
                            add("deploy");
                        }})
                        .call();
        assertTrue(new File(repository.getWorkTree(),
                "releases/org/docascode/test/myDoc1/1.0/myDoc1-1.0.docx").exists());
        assertTrue(new File(repository.getWorkTree(),
                "releases/org/docascode/test/myDoc1/1.0/myDoc1-1.0-beamer.pptx").exists());
        assertTrue(new File(repository.getWorkTree(),
                "releases/org/docascode/test/myDoc1/1.0/myDoc1-1.0-matrix.xlsx").exists());
        assertTrue(new File(repository.getWorkTree(),
                "releases/org/docascode/test/myDoc2/1.0/myDoc2-1.0.docx").exists());
        assertTrue(new File(repository.getWorkTree(),
                "releases/org/docascode/test/myDoc2/1.0/myDoc2-1.0-beamer.pptx").exists());
        assertTrue(new File(repository.getWorkTree(),
                "releases/org/docascode/test/myDoc2/1.0/myDoc2-1.0-matrix.xlsx").exists());

    }
}
