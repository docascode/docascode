package org.docascode.ant;

import org.docascode.api.BuildCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.DocAsCodeRepositoryTestCase;
import org.docascode.api.core.errors.DocAsCodeException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConvertTaskTest extends DocAsCodeRepositoryTestCase {
    @BeforeEach
    public void setUp(TestInfo testInfo) throws IOException, GitAPIException, DocAsCodeException {
        super.setUp(testInfo);
    }

    @Test
    void testConvert() throws DocAsCodeException {
        ((BuildCommand) DocAsCode.open(repository.getWorkTree())
                .build()
                .addListener(this))
                .setTargets(new ArrayList<String>(){{
                    add("nestedConvert");
                }})
                .call();
        File myDoc1 = new File(repository.getWorkTree(),"build/AAAA-MM-JJ_myDoc1-Title_v1.0.pdf");
        File myDoc2 = new File(repository.getWorkTree(),"build/AAAA-MM-JJ_myDoc2-Title_v1.0.pdf");
        assertTrue(myDoc1.exists());
        assertTrue(myDoc2.exists());
    }
}
