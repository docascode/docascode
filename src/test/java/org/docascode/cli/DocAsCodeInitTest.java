package org.docascode.cli;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.runner.RunWith;
import picocli.CommandLine;
import java.io.File;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class DocAsCodeInitTest {
    @Test
    @Parameters
    public void initTest(@TempDir Path tempDir, DocAsCode docascode, String[] args){
        tempDir.toFile().mkdirs();
        new CommandLine(docascode).run(docascode,args);
        File dir = new File(args[1]);
        assertTrue (new File(dir,"delivery.xml").exists());
        assertTrue (new File(dir,"delivery.properties").exists());
        assertTrue (new File(dir,".docascode/chrono.xml").exists());
        assertTrue (new File(dir,".docascode/config").exists());
        tempDir.toFile().delete();
    }

    private Object[] parametersForInitTest() {
        File file1 = new File("target/test-run/init1/");
        String[] args1 = {"init","target/test-run/init1"};

        File file2 = new File("target/test-run/");
        String[] args2 = {"init","target/test-run/init2"};
        return new Object[]{
                new Object[]{file1.toPath(), new DocAsCode(), args1},
                new Object[]{file2.toPath(), new DocAsCode(), args2}
        };
    }
}
