package org.docascode.api;

import org.docascode.api.core.DocAsCodeRepository;
import org.docascode.api.core.errors.DocAsCodeException;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class InitCommandTest extends DocAsCodeRepositoryTestCase {
    @Test
    void testInitFromDocAsCode_3_0() throws IOException, GitAPIException, DocAsCodeException {
        Git.open(repository.getWorkTree())
                .checkout()
                .setCreateBranch(true)
                .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                .setName("support-docascode-3")
                .setStartPoint("origin/support-docascode-3")
                .call();
        DocAsCodeRepository repo = DocAsCode.init()
                .setDirectory(repository.getWorkTree())
                .call()
                .getRepository();
        assert(!Files.exists(Paths.get(
                String.format("%s/chrono.xml",repository.getWorkTree()))));
        assert(Files.exists(Paths.get(
                repo.getChronoXML().getAbsolutePath())));
    }
}
