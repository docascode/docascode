package org.docascode.api;

import org.apache.commons.io.FileUtils;
import org.docascode.api.core.chrono.AttachMap;
import org.docascode.api.core.chrono.OutputMap;
import org.docascode.api.core.chrono.generated.Artifact;
import org.docascode.api.core.chrono.generated.BaseArtifact;
import org.docascode.api.core.chrono.generated.Maven;
import org.docascode.api.core.errors.DocAsCodeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;

class AddCommandTest {
    private File directory;
    private DocAsCode docascode;

    @BeforeEach
    void setUp() throws IOException, DocAsCodeException {
        directory = new File("target/test-run/add").getAbsoluteFile();
        FileUtils.deleteDirectory(directory);
        docascode = DocAsCode.init()
                .setDirectory(directory)
                .call();
    }

    @Test
    void addTest() throws DocAsCodeException {
        //add Minimal artifact
        Artifact a = docascode.add()
                .setID("id",null)
                .setFile(null)
                .setMaven(null,null,null)
                .setOutput(null,null)
                .call();
        Artifact expectedArtifact = new Artifact();
        AttachMap attached = new AttachMap();
        OutputMap outputsMain = new OutputMap();
        expectedArtifact.setAttach(attached);
        expectedArtifact.setOutputs(outputsMain);
        assertEquals(a,expectedArtifact);

        //add main artifact
        a = docascode.add()
                .setID("id",null)
                .setFile(new File(directory.getAbsolutePath()+"/folder/test-id.docx"))
                .setMaven(null,null,null)
                .setOutput("main","output.docx")
                .call();
        expectedArtifact.getOutputs().put("main","output.docx");
        expectedArtifact.setFile("folder/test-id.docx");
        assertEquals(a,expectedArtifact);

        //add minimal classifier and nexus informations
        a = docascode.add()
                .setID("id","classifier")
                .setMaven("group","artifact","version")
                .call();
        Maven n = new Maven();
        BaseArtifact expectedBaseArtifact = new BaseArtifact();
        OutputMap outputs = new OutputMap();
        expectedBaseArtifact.setOutputs(outputs);
        expectedArtifact.getAttach().put("classifier",expectedBaseArtifact);
        n.setArtifactId("artifact");
        n.setGroupId("group");
        n.setVersion("version");
        expectedArtifact.setMaven(n);
        assertEquals(a,expectedArtifact);


        a = docascode.add()
                .setID("id","classifier")
                .setFile(new File(directory.getAbsolutePath()+"/folder/test-id-classifier.docx"))
                .setMaven("group2","artifact2","version2")
                .setOutput("default","output.docx")
                .call();
        expectedArtifact.getAttach()
                .get("classifier").getOutputs().put("default","output.docx");
        expectedArtifact.getAttach()
                .get("classifier").setFile("folder/test-id-classifier.docx");
        n = new Maven();
        n.setArtifactId("artifact2");
        n.setGroupId("group2");
        n.setVersion("version2");
        expectedArtifact.setMaven(n);
        assertEquals(a,expectedArtifact);
    }
}
