package org.docascode.api.core.office;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.Substitute;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnitParamsRunner.class)
public class SubstituteTest {
    @Test
    @Parameters
    public void substituteTest(List<String> strings, HashMap<String,String> dict, HashMap<String,String> expected, Exception expectedException) {
        if (expectedException != null){
            assertThrows(expectedException.getClass(),()-> Substitute.substitute(strings,dict));
        } else {
            try {
                final Map<String,String> actual = Substitute.substitute(strings,dict);
                assertEquals(expected,actual);
            } catch (DocAsCodeException e) {
                fail();
            }

        }
    }

    private Object[] parametersForSubstituteTest() {
        HashMap<String,String> dict1 = new HashMap<String,String>();
        dict1.put("key1","value1");
        dict1.put("key2","value2");
        List<String> strings1 = new ArrayList<>();
        HashMap<String,String> expected1 = new HashMap<String,String>();
        strings1.add("@key1@-text-@key2@");
        expected1.put("@key1@-text-@key2@","value1-text-value2");
        strings1.add("accordeon");
        expected1.put("accordeon","accordeon");

        HashMap<String,String> dict2 = new HashMap<String,String>();
        dict1.put("key1","value1");
        List<String> strings2 = new ArrayList<>();
        strings2.add("@key1@-text-@key2@");
        return new Object[] {
                new Object[] {strings1, dict1,expected1,null},
                new Object[] {strings2, dict2,null, new DocAsCodeException("")}
        };
    }
}