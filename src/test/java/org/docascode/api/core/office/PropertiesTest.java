package org.docascode.api.core.office;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.docascode.api.core.errors.DocAsCodeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.File;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class PropertiesTest {
    @Test
    @Parameters
    public void listTest(File file,HashMap<String,String> expected) throws DocAsCodeException {
        assertEquals(expected, new PropertiesProcessor().list(file));
    }

    private Object[] parametersForListTest() {
        HashMap<String,String> expected1 = new HashMap<>();
        expected1.put("Title","Title 001");
        expected1.put("version","2.00");
        expected1.put("autre","test");
        File file1 = new File("src/test/resources/org/docascode/office/FICHIER_001.docx");
        return new Object[] {
                new Object[] {file1,expected1}
        };
    }
}
