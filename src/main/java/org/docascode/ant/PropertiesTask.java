package org.docascode.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Property;
import org.docascode.api.DocAsCode;
import org.docascode.api.UpdateCommand;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.listener.EventListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PropertiesTask extends Task implements EventListener {
    private List<File> files = new ArrayList<>();
    public void setFile(File file){
        this.files.add(file);
    }

    private String prefix = null;
    public void setPrefix(String prefix){
        this.prefix=prefix;
    }

    @Override
    public void execute() {
        Map<String, Map<String, String>> globalMap;
        try {
            globalMap = DocAsCode.update()
                    .setAction(UpdateCommand.Action.LIST)
                    .setFiles(files)
                    .call();
        } catch (DocAsCodeException e) {
            throw new BuildException(e);
        }
        Property p = new Property();
        p.setProject(getProject());
        for (Map.Entry<String,String> entry : globalMap.get(files.get(0).getAbsolutePath()).entrySet()){
            if (prefix!=null){
                p.setName(prefix+"."+entry.getKey());
            } else {
                p.setName(entry.getKey());
            }
            p.setValue(entry.getValue());
            p.execute();
        }
    }

    @Override
    public void fireEvent(Event event) {
        log(event.getMessage());
    }
}
