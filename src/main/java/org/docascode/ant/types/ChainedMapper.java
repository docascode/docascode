package org.docascode.ant.types;

import org.apache.tools.ant.util.ContainerMapper;

import java.util.Objects;
import java.util.stream.Stream;

public class ChainedMapper extends ContainerMapper {
    @Override
    public String[] mapFileName(String filename) {
        String[] result = getMappers().stream()
                .filter(Objects::nonNull)
                .filter(x -> {
                    if (x instanceof PropertyMapper){
                        ((PropertyMapper) x).setFile(filename);
                    }
                    return true;
                })
                .reduce(new String[] { filename },
                        (i, m) -> Stream.of(i)
                                .map(m::mapFileName)
                                .filter(Objects::nonNull)
                                .flatMap(Stream::of)
                                .toArray(String[]::new),
                        (i, o) -> o);
        return result == null || result.length == 0 ? null : result;
    }
}
