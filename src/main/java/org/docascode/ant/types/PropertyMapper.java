package org.docascode.ant.types;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.DataType;
import org.apache.tools.ant.util.FileNameMapper;
import org.docascode.api.core.Substitute;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.office.PropertiesProcessor;

import java.io.File;
import java.util.Map;

public class PropertyMapper extends DataType implements FileNameMapper {
    /**
     * Ignored.
     * @param from ignored.
     */
    @Override
    public void setFrom(String from) {
        throw new UnsupportedOperationException();
    }

    /**
     * Ignored.
     * @param to ignored.
     */
    @Override
    public void setTo(String to) {
        throw new UnsupportedOperationException();
    }

    private File file;

    public void setFile(String filename){

        this.file= new File(getProject().getBaseDir(),filename);
    }

    @Override
    public String[] mapFileName(String filename) {
        try {
            Map<String,String> dict = new PropertiesProcessor().list(file);
            return new String[]{Substitute.substitute(filename,dict)};
        } catch (DocAsCodeException e) {
            log(String.format(
                    "Cannot evaluate '%s' for file '%s'. Cause: '%s'",
                    filename,
                    file,
                    e.getMessage()), Project.MSG_WARN);
            throw new BuildException(e);
        }
    }
}
