package org.docascode.ant.types;

import org.apache.commons.io.FilenameUtils;
import org.apache.tools.ant.types.DataType;
import org.docascode.api.core.Substitute;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.office.PropertiesProcessor;
import org.eclipse.aether.artifact.DefaultArtifact;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Artifact extends DataType {
    private String groupId;
    public void setGroupId(String groupId){
        this.groupId = groupId;
    }

    private String artifactId;
    public void setArtifactId(String artifactId){
        this.artifactId = artifactId;
    }

    private String version;
    public void setVersion(String version){
        this.version = version;
    }

    private String extension;
    public void setExtension(String extension){
        this.extension = extension;
    }

    private File file;
    public void setFile (File file){
        this.file = file;
    }

    private List<AttachedArtifact> attached = new ArrayList<>();
    public void addConfiguredAttachedArtifact(AttachedArtifact attachedArtifact){
        this.attached.add(attachedArtifact);
    }

    public org.docascode.api.core.mvn.Artifacts toArtifacts() throws DocAsCodeException {
        org.docascode.api.core.mvn.Artifacts result = new org.docascode.api.core.mvn.Artifacts();
        Map<String,String> dict = new PropertiesProcessor().list(file);
        if (file != null){
            this.extension = FilenameUtils.getExtension(file.getAbsolutePath());
        }
        org.eclipse.aether.artifact.Artifact artifact =
                new DefaultArtifact(Substitute.substitute(groupId,dict),
                        Substitute.substitute(artifactId,dict),
                        Substitute.substitute("",dict),
                        extension,
                        Substitute.substitute(version,dict));
            artifact=artifact.setFile(this.file);
        result.add(artifact);
        for (AttachedArtifact attachedArtifact : attached){
            artifact =
                    new DefaultArtifact(Substitute.substitute(groupId,dict),
                            Substitute.substitute(artifactId,dict),
                            Substitute.substitute(attachedArtifact.getClassifier(),dict),
                            attachedArtifact.getExtension(),
                            Substitute.substitute(version,dict));
            artifact=artifact.setFile(attachedArtifact.getFile());
            result.add(artifact);
        }
        return result;
    }
}
