package org.docascode.ant.types;

import org.apache.commons.io.FilenameUtils;
import org.apache.tools.ant.types.DataType;

import java.io.File;

public class AttachedArtifact extends DataType {
    private String classifier;
    public void setClassifier(String classifier){
        this.classifier = classifier;
    }
    String getClassifier(){
        return this.classifier;
    }

    private String extension;
    public void setExtension(String extension){
        this.extension = extension;
    }
    String getExtension(){
        if (file != null){
            this.extension = FilenameUtils.getExtension(file.getAbsolutePath());
        }
        return this.extension;
    }

    private File file;
    public void setFile (File file){
        this.file = file;
    }
    File getFile(){
        return this.file;
    }

}
