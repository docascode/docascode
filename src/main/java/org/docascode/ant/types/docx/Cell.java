package org.docascode.ant.types.docx;

import org.apache.tools.ant.types.DataType;

public class Cell extends DataType {
    private org.docascode.api.core.office.table.Cell coreCell = new org.docascode.api.core.office.table.Cell();

    public void setText(String text){
        this.coreCell.setText(text);
    }

    public void add(ParagraphFormat paragraphFormat){
        this.coreCell.setParagraphFormat(paragraphFormat.toCore());
    }

    org.docascode.api.core.office.table.Cell toCore(){
        return this.coreCell;
    }
}
