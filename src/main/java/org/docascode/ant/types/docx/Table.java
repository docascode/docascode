package org.docascode.ant.types.docx;

import org.apache.tools.ant.types.DataType;

public class Table extends DataType {
    private org.docascode.api.core.office.table.Table coreTable = new org.docascode.api.core.office.table.Table();

    public void setNum(int num){
        this.coreTable.setNum(num);
    }

    public void addRow(Row row){
        this.coreTable.add(row.toCore());
    }

    public void add(ParagraphFormat paragraphFormat){
        this.coreTable.setParagraphFormat(paragraphFormat.toCore());
    }

    public org.docascode.api.core.office.table.Table toCore(){
        return this.coreTable;
    }

}
