package org.docascode.ant.types.docx;

import org.apache.tools.ant.types.DataType;

public class Row extends DataType {
    private org.docascode.api.core.office.table.Row coreRow = new org.docascode.api.core.office.table.Row();

    public void add(Cell cell){
        this.coreRow.add(cell.toCore());
    }

    public void add(ParagraphFormat paragraphFormat){
        this.coreRow.setParagraphFormat(paragraphFormat.toCore());
    }

    org.docascode.api.core.office.table.Row toCore(){
        return this.coreRow;
    }
}
