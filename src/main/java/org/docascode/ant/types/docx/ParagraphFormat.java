package org.docascode.ant.types.docx;

import org.apache.tools.ant.types.DataType;
import org.docx4j.wml.JcEnumeration;

public class ParagraphFormat extends DataType {
    private org.docascode.api.core.office.ParagraphFormat coreParagraphFormat = new org.docascode.api.core.office.ParagraphFormat();

    public void setJustification(JcEnumeration jcEnumeration){
        this.coreParagraphFormat.setJcEnumeration(jcEnumeration);
    }

    public void setSpaceAfter(long spaceAfter){
        this.coreParagraphFormat.setSpaceAfter(spaceAfter);
    }

    org.docascode.api.core.office.ParagraphFormat toCore() {
        return this.coreParagraphFormat;
    }
}
