package org.docascode.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.docascode.ant.types.Artifact;
import org.docascode.api.DeployCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.mvn.Artifacts;
import org.docascode.api.event.Event;
import org.docascode.api.listener.EventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeployTask extends Task implements EventListener {
    private List<Artifacts> artifacts = new ArrayList<>();
    public void addConfiguredArtifact(Artifact artifact) throws DocAsCodeException {
        this.artifacts.add(artifact.toArtifacts());
    }

    private List<String> ids = new ArrayList<>();
    public void setIds(String ids){
        if (!ids.equals("")) {
            this.ids = Arrays.asList(ids.split(","));
        }
    }

    private String profile = "default";
    public void setProfile(String profile){
        this.profile=profile;
    }

    @Override
    public void execute() {
        try (DocAsCode docascode = DocAsCode.open(getProject().getBaseDir())){
            ((DeployCommand) docascode.deploy().addListener(this))
                    .addIds(this.ids)
                    .addArtifacts(this.artifacts)
                    .setProfile(profile)
                    .call()
                    .removeListener(this);
        } catch (DocAsCodeException e) {
            throw new BuildException(e);
        }
    }

    @Override
    public void fireEvent(Event event) {
        log(event.getMessage());
    }
}
