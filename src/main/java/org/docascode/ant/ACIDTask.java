package org.docascode.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.docascode.api.ACIDCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.listener.EventListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ACIDTask extends Task implements EventListener {
    private String destDir = "bin";
    public void setDestDir(String destDir){
        this.destDir = destDir;
    }

    private String baseName = "archive";
    public void setBaseName(String baseName){
        this.baseName = baseName;
    }

    private List<File> pubKeys = new ArrayList<>();
    public void setPubKeyDir(String pubKeyDir){
        FileSet fileSet = new FileSet();
        fileSet.setDir(new File (
                getProject().getBaseDir(),
                pubKeyDir));
        fileSet.setIncludes("**/*.acidppr");
        final DirectoryScanner scanner =
                fileSet.getDirectoryScanner(getProject());
        for (String directory : scanner.getIncludedDirectories()) {
            if (!directory.equals("")){
                final File folder =
                        new File(fileSet.getDir(getProject()), directory);
                this.pubKeys.add(folder);
            }
        }
        for (String filename : scanner.getIncludedFiles()) {
            final File file =
                    new File(fileSet.getDir(getProject()), filename);
            this.pubKeys.add(file);
        }
    }

    private List<File> files = new ArrayList<>();
    public void setDir(String dir){
        FileSet fileSet = new FileSet();
        fileSet.setDir(new File (getProject().getBaseDir(),
            dir));
        fileSet.setIncludes("*");
        final DirectoryScanner scanner =
            fileSet.getDirectoryScanner(getProject());
        for (String directory : scanner.getIncludedDirectories()) {
            if (!directory.equals("")){
                final File folder =
                        new File(fileSet.getDir(getProject()), directory);
                this.files.add(folder);
            }
        }
        for (String filename : scanner.getIncludedFiles()) {
            final File file =
                    new File(fileSet.getDir(getProject()), filename);
            this.files.add(file);
        }
    }

    @Override
    public void execute() {
        try {
            ((ACIDCommand) DocAsCode.acid().addListener(this))
                    .setDestDir(new File(getProject().getBaseDir(),this.destDir))
                    .setName(this.baseName)
                    .setPubKeys(this.pubKeys)
                    .setFiles(this.files)
                    .call();
        } catch (DocAsCodeException e) {
            throw new BuildException(e);
        }
    }

    @Override
    public void fireEvent(Event event) {
        log(event.getMessage());
    }
}
