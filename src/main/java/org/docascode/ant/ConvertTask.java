package org.docascode.ant;

import org.apache.commons.io.FilenameUtils;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.Mapper;
import org.apache.tools.ant.util.FileNameMapper;
import org.apache.tools.ant.util.IdentityMapper;
import org.docascode.api.ConvertCommand;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;
import org.docascode.api.listener.EventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ConvertTask extends Task implements EventListener {
    private String destDir = ".";
    public void setDestDir(String destDir){
        this.destDir = destDir;
    }

    private FileSet fileSet = new FileSet();
    public void addFileSet(final FileSet fileset){
        this.fileSet = fileset;
        this.fileSet.setExcludes("*.pdf");
    }

    public Mapper createMapper() {
        if (elementMapper != null) {
            throw new BuildException("Cannot define more than one mapper",
                    getLocation());
        }
        elementMapper = new Mapper(getProject());
        return elementMapper;
    }

    private Mapper elementMapper;
    public void add(FileNameMapper mapper){
        createMapper().add(mapper);
    }

    private FileNameMapper getMapper() {
        FileNameMapper mapper;
        if (elementMapper != null) {
            mapper = elementMapper.getImplementation();
        } else {
            mapper = new IdentityMapper();
        }
        return mapper;
    }

    @Override
    public void execute() {
        final DirectoryScanner scanner =
                fileSet.getDirectoryScanner(getProject());
        String[] includedFileNames = scanner.getIncludedFiles();
        for (String filename : includedFileNames) {
            final File fromFile =
                    new File(fileSet.getDir(getProject()), filename);
            List<String> toFileNames = new ArrayList<>();
            if ( getMapper().mapFileName(filename) != null) {
                for (String toFileName : getMapper().mapFileName(filename)) {
                    toFileNames.add(
                            String.format(
                                    "%s.pdf",
                                    FilenameUtils.removeExtension(toFileName)));
                }
            }
            for (String toFileName : toFileNames){
                File toFile = new File(
                        getProject().getBaseDir(),
                        String.format("%s/%s",
                                destDir,
                                toFileName));
                try {
                    ((ConvertCommand) DocAsCode.convert().addListener(this))
                            .from(fromFile)
                            .to(toFile)
                            .call();
                } catch (DocAsCodeException e) {
                    throw new BuildException(e);
                }
            }
        }
    }

    @Override
    public void fireEvent(Event event) {
        log(event.getMessage());
    }
}
