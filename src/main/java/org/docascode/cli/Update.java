package org.docascode.cli;

import org.docascode.api.UpdateCommand;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;
import java.io.File;
import java.util.*;

import org.docascode.api.DocAsCode;

@CommandLine.Command(name = "update",
    description="Create or update Custom properties of OpenXML files.")
public class Update extends Command implements Runnable {
    @CommandLine.Option(names = {"--file","-f"} , arity = "1", description = "The files to act on")
    private File file;

    @CommandLine.Option(names = {"--list","-l"}, description = "List all properties of the given file")
    private boolean list = false;

    @CommandLine.Option(names = {"--delete","-d"}, description = "The properties to delete")
    private List<String> delete = new ArrayList<>();

    @CommandLine.Option(names = {"--properties","-p"}, description = "The properties to set/update")
    private Map<String, String> properties = new HashMap<>();

    @Override
    public void run() {
        UpdateCommand update = DocAsCode.update()
                .setFiles(Arrays.asList(file));
        if (list){
            update.setAction(UpdateCommand.Action.LIST);
        } else {
            update.setProperties(properties)
                .deleteProperties(delete)
                .setAction(UpdateCommand.Action.ADD);
        }
        try {
            ((UpdateCommand) update.addListener(this)).call();
        } catch (DocAsCodeException e) {
            error(e);
        }
    }
}
