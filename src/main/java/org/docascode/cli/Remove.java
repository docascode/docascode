package org.docascode.cli;

import org.docascode.api.DocAsCode;
import org.docascode.api.RemoveCommand;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

@CommandLine.Command(name = "remove", aliases = {"rm"},
        description="Remove entries from the DocAsCode repository Database")
public class Remove extends Command implements Runnable {
    @CommandLine.Option(names = {"--file","-f"}, description = "Remove the file informations")
    private boolean file = false;

    @CommandLine.Option(names = {"--gav, --maven"}, description = "Remove Maven informations")
    private boolean maven = false;

    @CommandLine.Option(names = {"--id","-i"}, description = "The identifier of artifact")
    private String id;

    @CommandLine.Option(names = {"--classifier","-c"}, description = "The identifier of the attached artifact to remove")
    private String classifier = null;

    @CommandLine.Option(names = {"--name","-n"}, description = "Remove the specified naming rule")
    private String outputName = null;

    @CommandLine.Option(names = {"--outputs","-o"}, description = "Remove all rules for output naming")
    private boolean outputs = false;

    @CommandLine.Option(names = {"--all","-a"}, description = "Remove all attached artifacts.")
    private boolean all = false;

    @CommandLine.Option(names = {"--entire","-e"}, description = "Remove the entire artifact or attached artifact.")
    private boolean entire = false;

    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()) {
            RemoveCommand cmd = docascode.remove()
                    .setId(id, classifier)
                    .removeAllOuputs(outputs)
                    .removeGAV(maven)
                    .removeFile(file)
                    .removeOutput(outputName)
                    .removeAllClassifiers(all);
            if (entire) {
                cmd.removeAllOuputs(true)
                        .removeGAV(true)
                        .removeFile(true)
                        .removeAllClassifiers(true);
            }
            cmd.call();
        } catch (DocAsCodeException e) {
            error(e);
        }
    }
}
