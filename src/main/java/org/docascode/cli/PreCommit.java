package org.docascode.cli;

import org.docascode.api.DocAsCode;
import org.docascode.api.PreCommitCommand;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

@CommandLine.Command(name = "pre-commit", hidden = true,
    description="Generate preview of all staged files.")
public class PreCommit extends Command implements Runnable {
    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()) {
            ((PreCommitCommand)docascode
                    .preCommit()
                    .addListener(this))
                    .call();
        } catch (DocAsCodeException e) {
            error(e);
        }
    }
}
