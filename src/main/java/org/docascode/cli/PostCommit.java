package org.docascode.cli;

import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

@CommandLine.Command(name = "post-commit", hidden = true,
    description="Clean-up last commit.")
public class PostCommit extends Command implements Runnable {
    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()){
            docascode.postCommit()
                        .call();
        } catch (DocAsCodeException e) {
            error(e);
        }
    }
}
