package org.docascode.cli;

import org.docascode.api.DocAsCode;
import org.docascode.api.MergeCommand;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

@CommandLine.Command(name = "merge", hidden=true,
    description = "Perfoms a 3-way merge.")
public class Merge extends Command implements Runnable {
    @CommandLine.Option(names = {"--format","-f"}, arity = "1")
    private String format;

    @CommandLine.Option(names = {"--ancestor", "-a"}, arity = "1")
    private String ancestor;

    @CommandLine.Option(names = {"--other", "-o"}, arity = "1")
    private String other;

    @CommandLine.Option(names = {"--current", "-c"}, arity = "1")
    private String current;

    @CommandLine.Option(names = {"--placeholder", "-p"}, arity = "1")
    private String placeHolder;

    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()){
            ((MergeCommand) docascode.merge().addListener(this))
                    .setAncestor(ancestor)
                    .setCurrent(current)
                    .setOther(other)
                    .setPlaceHolder(placeHolder)
                    .setFormat(format)
                    .call();
            System.exit(1);
        } catch (DocAsCodeException e){
            error(e);
        }
    }
}