package org.docascode.cli;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;
import org.docascode.ant.DocAsCodeAntLogger;
import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CommandLine.Command(name = "build",
    description = "Performs the build defined in delivery.xml.")
public class Build extends Command implements Runnable  {
    @CommandLine.Option(names = {"-t", "--target"}, arity = "0..*", description = "The Targets to run.")
    private List<String> targets = new ArrayList<>();

    @CommandLine.Option(names = {"-p", "--property"}, description = "Define properties to inject in your build.")
    private Map<String, String> properties  = new HashMap<>();

    @CommandLine.Option(names = {"-l", "--list"}, description = "List the targets defined in delivery.xml.")
    private Boolean list = false;

    @CommandLine.Option(names = {"--log-level"}, description = "Set the output log-level. Valid values: ${COMPLETION-CANDIDATES}")
    private LogLevel logLevel  = LogLevel.INFO;

    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()) {
            if (list){
                Map<String, Target> listTargets = docascode.build()
                        .listTargets();
                for ( Map.Entry<String, Target> entry : listTargets.entrySet()){
                    success(
                            String.format("%s (defined in %s)%n",
                                    entry.getKey(),
                                    entry.getValue().getLocation().getFileName())
                    );
                    info(
                            String.format("    %s%n",
                                    entry.getValue().getDescription() == null ? "No Description" : entry.getValue().getDescription())
                    );
                }

            } else {
                DocAsCodeAntLogger docAsCodeAntLogger = new DocAsCodeAntLogger();
                docAsCodeAntLogger.setMessageOutputLevel(this.logLevel.getAntLogLevel());
                docascode.build()
                        .setTargets(targets)
                        .addLogger(docAsCodeAntLogger)
                        .setProperties(properties)
                        .call();
                System.exit(0);
            }
        } catch (DocAsCodeException e) {
            error(e);
        } catch (BuildException e){
            error(e);
            System.exit(1);
        }
    }

    public enum LogLevel{
        INFO(Project.MSG_INFO),
        WARN(Project.MSG_WARN),
        DEBUG(Project.MSG_DEBUG);

        private int antLogLevel;

        LogLevel(int antLogLevel) {
            this.antLogLevel = antLogLevel;
        }

        int getAntLogLevel(){
            return this.antLogLevel;
        }
    }
}
