package org.docascode.cli;

import org.docascode.api.DocAsCode;
import org.docascode.api.core.errors.DocAsCodeException;
import picocli.CommandLine;
import java.io.File;

@CommandLine.Command(name = "add",
    description = "Add entries to DocAsCode repository Database")
public class Add extends Command implements Runnable {
    @CommandLine.Option(names = {"--file","-f"} , arity = "1", description = "The file to add")
    private File file;

    @CommandLine.Option(names = {"--id","-i"}, description = "The identifier of the file")
    private String id;

    @CommandLine.Option(names = {"--classifier","-c"}, description = "The identifier of the file to attach")
    private String classifier = null;

    @CommandLine.Option(names = {"--name","-n"}, description = "The identifier of the output field")
    private String outputName = "default";

    @CommandLine.Option(names = {"--output","-o"}, description = "Rule for output naming")
    private String output;

    @CommandLine.Option(names = {"--groupID","-g"}, description = "(Maven) groupID of the artifact")
    private String groupID;

    @CommandLine.Option(names = {"--artifactID","-a"}, description = "(Maven) name of the artifact")
    private String artifactID;

    @CommandLine.Option(names = {"--version","-v"}, description = "(Maven) version of the artifact")
    private String version;

    @Override
    public void run() {
        try (DocAsCode docascode = DocAsCode.open()){
            docascode.add().setID(id, classifier)
                        .setFile(file)
                        .setMaven(groupID, artifactID, version)
                        .setOutput(outputName, output)
                        .call();
        } catch (DocAsCodeException e){
            error(e);
        }
    }
}
