package org.docascode.api.core.mvn;

import org.eclipse.aether.artifact.Artifact;

import java.util.*;

public class Artifacts {
    private Artifact mainArtifact;

    private List<Artifact> attachedArtifacts = new ArrayList<>();

    public Artifacts add(Artifact artifact){
        if (artifact.getClassifier().equals("")){
            this.mainArtifact = artifact;
        } else {
            this.attachedArtifacts.add(artifact);
        }
        return this;
    }

    Artifact getMainArtifact(){
        return this.mainArtifact;
    }

    List<Artifact> getArtifacts(){
        List<Artifact> result = new ArrayList<>();
        result.add(this.mainArtifact);
        result.addAll(this.attachedArtifacts);
        return result;
    }
}
