package org.docascode.api.core.office.table;

import org.docascode.api.core.office.ParagraphFormat;
import org.docx4j.jaxb.Context;
import org.docx4j.wml.*;

public class Cell {
    private String text;

    private ParagraphFormat paragraphFormat;

    public Cell setText(String text){
        this.text = text;
        return this;
    }

    public Cell setParagraphFormat(ParagraphFormat paragraphFormat){
        if (this.paragraphFormat == null){
            this.paragraphFormat = paragraphFormat;
        }
        return this;
    }

    P toP(){
        org.docx4j.wml.ObjectFactory factory = Context.getWmlObjectFactory();
        P p = factory.createP();
        if (this.paragraphFormat != null){
            p.setPPr(this.paragraphFormat.toPPr());
        }
        R r = factory.createR();
        p.getContent().add(r);
        Text t = factory.createText();
        t.setValue(this.text);
        r.getContent().add(t);
        return p;
    }
}
