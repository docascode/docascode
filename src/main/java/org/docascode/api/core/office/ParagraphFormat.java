package org.docascode.api.core.office;

import org.docx4j.jaxb.Context;
import org.docx4j.wml.Jc;
import org.docx4j.wml.JcEnumeration;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase;

import java.math.BigInteger;

public class ParagraphFormat {
    private JcEnumeration jcEnumeration = JcEnumeration.CENTER;

    private long spaceAfterSize = 0;

    public ParagraphFormat setJcEnumeration(JcEnumeration jcEnumeration){
        this.jcEnumeration = jcEnumeration;
        return this;
    }

    public ParagraphFormat setSpaceAfter(long spaceAfter){
        this.spaceAfterSize = spaceAfter;
        return this;
    }

    public PPr toPPr(){
        org.docx4j.wml.ObjectFactory factory = Context.getWmlObjectFactory();
        PPr paragraphProperties = factory.createPPr();

        Jc justification = factory.createJc();
        justification.setVal(jcEnumeration);
        paragraphProperties.setJc(justification);

        PPrBase.Spacing space = factory.createPPrBaseSpacing();
        space.setAfter(BigInteger.valueOf(spaceAfterSize));
        space.setAfterAutospacing(false);
        paragraphProperties.setSpacing(space);

        return paragraphProperties;
    }
}
