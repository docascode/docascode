package org.docascode.api.core.converter;
import org.docascode.api.core.errors.ProcessingException;
import org.jodconverter.core.document.DefaultDocumentFormatRegistry;
import org.jodconverter.core.office.OfficeException;
import org.jodconverter.core.office.OfficeManager;
import org.jodconverter.core.office.OfficeUtils;
import org.jodconverter.local.JodConverter;
import org.jodconverter.local.office.LocalOfficeManager;

import java.io.*;

public class PptxToPdfDriver extends AbstractConverter{
    @Override
    public void convert() throws ProcessingException {
        if (!toFile.getParentFile().exists() && !toFile.getParentFile().mkdirs()){
            throw new ProcessingException(
                String.format("Unable to create directory '%s'.",toFile.getParentFile()));
        }


            OfficeManager officeManager = LocalOfficeManager.builder()
                    .install()
                    .officeHome(System.getenv("LIBREOFFICE_HOME"))
                    .build();

        try (OutputStream os = new FileOutputStream(toFile);
             InputStream is = new FileInputStream(fromFile)){
            officeManager.start();
                JodConverter
                        .convert(is)
                        .as(DefaultDocumentFormatRegistry.PPTX)
                        .to(os)
                        .as(DefaultDocumentFormatRegistry.PDF)
                        .execute();
        } catch (IOException | OfficeException e) {
            throw new ProcessingException(
                    String.format(
                            "Unable to convert '%s' to '%s",
                            fromFile,
                            toFile),e);
            } finally {
                OfficeUtils.stopQuietly(officeManager);
        }
    }
}
