package org.docascode.api.core.converter;

import org.docascode.api.event.Event;

public class NotSupportedConversionDriver extends AbstractConverter {
    @Override
    public void convert() {
        log(String.format(
                "No supported conversion driver for converting '%s' to '%s'.",
                fromFile,
                toFile),
                Event.Level.INFO);
    }
}
