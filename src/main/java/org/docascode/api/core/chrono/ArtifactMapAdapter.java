package org.docascode.api.core.chrono;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import org.docascode.api.core.chrono.generated.Artifact;
import org.docascode.api.core.chrono.generated.Artifacts;

import java.util.Map;

public class ArtifactMapAdapter extends XmlAdapter<Artifacts, ArtifactMap> {

    @Override
    public ArtifactMap unmarshal(Artifacts artifacts) {
        ArtifactMap map = new ArtifactMap();
        for (Artifacts.Artifact e : artifacts.getArtifact())
        {
            Artifact a = new Artifact();
            a.setMaven(e.getMaven());
            a.setFile(e.getFile());
            a.setAttach(e.getAttach());
            a.setOutputs(e.getOutputs());
            map.put(e.getId(), a);
        }
        return map;
    }

    @Override
    public Artifacts marshal(ArtifactMap map) {
        if (map == null || map.entrySet().isEmpty()) {
            return null;
        } else {
            Artifacts modeller = new Artifacts();
            for (Map.Entry<String, Artifact> entry : map.entrySet()) {
                Artifacts.Artifact a = new Artifacts.Artifact();
                a.setId(entry.getKey());
                a.setMaven(entry.getValue().getMaven());
                a.setAttach(entry.getValue().getAttach());
                a.setOutputs(entry.getValue().getOutputs());
                a.setFile(entry.getValue().getFile());
                modeller.getArtifact().add(a);
            }
            return modeller;
        }
    }
}
