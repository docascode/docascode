package org.docascode.api.core.chrono;

import org.docascode.api.core.chrono.generated.Artifact;

import java.util.TreeMap;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlJavaTypeAdapter(ArtifactMapAdapter.class)
public class ArtifactMap extends TreeMap<String, Artifact> {

}
