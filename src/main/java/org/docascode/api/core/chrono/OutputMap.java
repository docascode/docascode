package org.docascode.api.core.chrono;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.TreeMap;

@XmlJavaTypeAdapter(OutputMapAdapter.class)
public class OutputMap  extends TreeMap<String,String> {

}
