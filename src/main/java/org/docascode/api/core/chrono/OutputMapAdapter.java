package org.docascode.api.core.chrono;

import org.docascode.api.core.chrono.generated.Outputs;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Map;

public class OutputMapAdapter extends XmlAdapter<Outputs, OutputMap> {
    @Override
    public OutputMap unmarshal(Outputs outputs) {
        OutputMap map = new OutputMap();
        for (Outputs.Output e : outputs.getOutput())
        {
            map.put(e.getName(), e.getValue());
        }
        return map;
    }

    @Override
    public Outputs marshal(OutputMap map) {
        if (map == null || map.entrySet().isEmpty()){
            return null;
        } else {
            Outputs modeller = new Outputs();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                Outputs.Output e = new Outputs.Output();
                e.setName(entry.getKey());
                e.setValue(entry.getValue());
                modeller.getOutput().add(e);
            }
            return modeller;
        }
    }
}
