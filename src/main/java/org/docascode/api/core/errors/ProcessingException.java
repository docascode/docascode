package org.docascode.api.core.errors;

public class ProcessingException extends DocAsCodeException {
    public ProcessingException(String s, Exception e) {
        super(s,e);
    }

    public ProcessingException(String message) {
        super(message);
    }
}
