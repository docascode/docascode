package org.docascode.api.core.errors;

public class DocAsCodeException extends Exception {
    public DocAsCodeException(String msg,Exception e) {
        super(msg, e);
    }

    public DocAsCodeException(String msg) {
        super(msg);
    }
}
