package org.docascode.api;

import org.docascode.api.core.DocAsCodeRepository;
import org.docascode.api.core.chrono.generated.*;
import org.docascode.api.core.chrono.*;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.errors.ProcessingException;
import org.docascode.api.event.Event;

import java.io.File;

public class AddCommand extends DocAsCodeCommand<Artifact> {
    private File file;

    private String id;

    private String classifier = null;

    private String outputName = null;

    private String output = null;

    private String groupID = null;

    private String artifactID = null;

    private String version = null;

    AddCommand(DocAsCodeRepository repo) {
        super(repo);
    }

    public AddCommand setFile(File location){
        return set(id,classifier,
                location,
                outputName,output,
                groupID,artifactID,version);
    }

    public AddCommand setOutput(String outputName, String output){
        return set(id,classifier,
                file,
                outputName,output,
                groupID,artifactID,version);
    }

    public AddCommand setMaven(String groupID, String artifactID, String version){
        return set(id,classifier,
                file,
                outputName,output,
                groupID,artifactID,version);
    }


    public AddCommand setID(String id, String classifier) {
        return set(id,classifier,
                file,
                outputName,output,
                groupID,artifactID,version);
    }

    public AddCommand set(String id,
                          String classifier,
                          File file,
                          String outputName,
                          String output,
                          String groupID,
                          String artifactID,
                          String version){
        this.id = id;
        this.classifier = classifier;
        this.file = file;
        this.outputName = outputName;
        this.output = output;
        this.groupID = groupID;
        this.artifactID = artifactID;
        this.version = version;
        return this;
    }

    private void sanityCheck() throws DocAsCodeException {
        if (id == null){
            throw new DocAsCodeException("Missing mandatory field 'id'.");
        }
        if (file == null || !file.exists()){
            log(String.format("The file '%s' doesn't exist.",this.file), Event.Level.WARN);
        }
    }

    private Artifact getArtifact(String id) throws ProcessingException {
        Delivery delivery = getRepository().chrono().getDelivery();
        Artifact a;
        if (delivery.getArtifacts() == null) {
            delivery.setArtifacts(new ArtifactMap());
        }
        if (delivery.getArtifacts().containsKey(id)) {
            a = delivery.getArtifacts().get(id);
        } else {
            a = new Artifact();
        }
        if (a.getAttach() == null) {
            a.setAttach(new AttachMap());
        }
        return a;
    }

    private void attach(Artifact a){
        BaseArtifact attach;
        if (a.getAttach().containsKey(this.classifier)){
            attach = a.getAttach().get(this.classifier);
        } else {
            attach = new BaseArtifact();
        }
        if (attach.getOutputs() == null) {
            attach.setOutputs(new OutputMap());
        }
        if (this.file != null) {
            attach.setFile(getRepository().relativize(this.file));
        } else {
            if (attach.getFile() == null || attach.getFile().equals("")){
                attach.setFile(null);
            }
        }
        if (this.output != null) {
            attach.getOutputs().put(this.outputName, this.output);
        }
        a.getAttach().put(this.classifier,attach);
    }

    private void attachGAV(Artifact a){
        if (version != null && groupID != null && artifactID!=null) {
            Maven n;
            if (a.getMaven() == null) {
                n = new Maven();
            } else {
                n = a.getMaven();
            }
            n.setVersion(version);
            n.setGroupId(groupID);
            n.setArtifactId(artifactID);
            a.setMaven(n);
        }
    }

    @Override
    public Artifact call() throws DocAsCodeException {
        sanityCheck();
        Delivery delivery = getRepository().chrono().getDelivery();
        Artifact a = getArtifact(this.id);
        if (this.classifier != null){
            attach(a);
        } else {
            if (a.getOutputs() == null) {
                a.setOutputs(new OutputMap());
            }
            if (this.file != null) {
                a.setFile(getRepository().relativize(this.file));
            } else {
                if (a.getFile() == null || a.getFile().equals("")){
                    a.setFile(null);
                }
            }
            if (this.output != null) {
                a.getOutputs().put(this.outputName, this.output);
            }
        }
        attachGAV(a);
        delivery.getArtifacts().put(this.id,a);
        getRepository().chrono().save(delivery);
        return a;
    }
}
