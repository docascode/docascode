package org.docascode.api;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.DocAsCodeRepository;
import org.docascode.api.event.Event;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
import org.eclipse.jgit.treewalk.filter.TreeFilter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class PreCommitCommand extends DocAsCodeCommand<Void> {
    private static final String PROCESSING_FILES = "Processing files";

    private static final String MARKDOWN = "markdown/%s.%s";

    PreCommitCommand(DocAsCodeRepository repo) {
        super(repo);
    }

    @Override
    public Void call() throws DocAsCodeException {
        if (!getRepository().git().hasEmptyTree()) {
            getRepository().git().lock();
            try (Git git = Git.open(getRepository().getWorkTree())){
                TreeFilter filter = PathSuffixFilter.create(".docx");
                List<DiffEntry> entries = git.diff()
                        .setCached(true)
                        .setPathFilter(filter)
                        .call();
                int total = entries.size();
                int progress = 0;
                for (DiffEntry entry : entries) {
                    File toMediaDirFile;
                    progress+=1;
                    switch (entry.getChangeType()) {
                        case ADD:
                        case MODIFY:
                            progress(progress, total,PROCESSING_FILES, Event.Level.INFO);
                            new ConvertCommand().from(new File(entry.getNewPath()))
                                    .to(new File(getRepository().getDocAsCodeDir(),
                                            String.format(
                                                    MARKDOWN,
                                                    FilenameUtils.removeExtension(entry.getNewPath()),
                                                    "md")))
                                    .call();
                            break;
                        case DELETE:
                            progress(progress, total,PROCESSING_FILES, Event.Level.INFO);
                            toMediaDirFile = new File(getRepository().getDocAsCodeDir(),
                                    String.format(
                                            "markdown/%s",
                                            FilenameUtils.removeExtension(entry.getOldPath())
                                    ));
                            deletePreview(toMediaDirFile);
                            break;
                        case RENAME:
                            progress(progress, total,PROCESSING_FILES, Event.Level.INFO);
                            new ConvertCommand().from(new File(entry.getNewPath()))
                                    .to(new File(getRepository().getDocAsCodeDir(),
                                            String.format(
                                                    MARKDOWN,
                                                    FilenameUtils.removeExtension(entry.getNewPath()),
                                                    "md")))
                                    .call();
                            toMediaDirFile = new File(getRepository().getDocAsCodeDir(),
                                    String.format(
                                            "markdown/%s",
                                            FilenameUtils.removeExtension(entry.getOldPath())
                                    ));
                            deletePreview(toMediaDirFile);
                            break;
                        default:
                            break;
                    }
                }
                if (total >0 ){
                    log(String.format(
                            "Processing files: 100%% (%s/%s), done",
                            total,total),
                            Event.Level.SUCESS);
                }
            } catch (GitAPIException | IOException e) {
                throw new DocAsCodeException("Unable to process pre-commit.", e);
            }
        }
        return null;
    }

    private void deletePreview(File toMediaDirFile){
        File mdFile = new File(toMediaDirFile, ".md");
        if (mdFile.exists()) {
            try {
                Files.delete(Paths.get(mdFile.getAbsolutePath()));
                FileUtils.deleteDirectory(toMediaDirFile);
            } catch (IOException e) {
                log(String.format(
                        "Failed to delete preview files '%s",mdFile.getAbsolutePath()
                ), Event.Level.WARN);
            }
        }
    }
}
