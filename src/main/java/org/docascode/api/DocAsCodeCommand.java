package org.docascode.api;

import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.core.DocAsCodeRepository;
import org.docascode.api.listener.APIEventListener;

import java.util.concurrent.Callable;

public abstract class DocAsCodeCommand<T> extends APIEventListener implements Callable<T> {
    private final DocAsCodeRepository repo;

    public DocAsCodeCommand(DocAsCodeRepository repo) {
        this.repo = repo;
    }

    public DocAsCodeCommand(){
        this(null);
    }

    protected DocAsCodeRepository getRepository(){
        return this.repo;
    }

    @Override
    public abstract T call() throws DocAsCodeException;

}
