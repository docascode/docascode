package org.docascode.api;

import org.docascode.api.core.DocAsCodeRepository;
import org.docascode.api.core.chrono.generated.*;
import org.docascode.api.core.errors.ProcessingException;

public class RemoveCommand extends DocAsCodeCommand<BaseArtifact> {
    private boolean file = false;

    private String id;

    private String classifier;

    private String outputName = null;

    private boolean outputs = false;

    private boolean nexus = false;

    private boolean allClassifiers = false;

    public RemoveCommand(DocAsCodeRepository repo) {
        super(repo);
    }

    public RemoveCommand removeAllClassifiers(boolean allClassifiers){
        return set(id,classifier,outputName,
                file,outputs, nexus,allClassifiers);
    }

    public RemoveCommand setId(String id, String classifier){
        return set(id,classifier,outputName,
                file,outputs, nexus,allClassifiers);
    }

    public RemoveCommand removeOutput(String name){
        return set(id,classifier,name,
                file,outputs,nexus,allClassifiers);
    }

    public RemoveCommand set(String id, String classifier, String outputName,
                             boolean file, boolean outputs, boolean nexus, boolean allClassifiers) {
        this.id = id;
        this.classifier = classifier;
        this.file = file;
        this.outputName = outputName;
        this.outputs = outputs;
        this.nexus = nexus;
        this.allClassifiers = allClassifiers;
        return this;
    }

    public RemoveCommand removeFile(boolean file){
        return set(id, classifier, outputName,
                file, outputs,nexus,allClassifiers);
    }


    public RemoveCommand removeAllOuputs(boolean outputs){
        return set(id, classifier, outputName,
                file, outputs,nexus,allClassifiers);
    }

    public RemoveCommand removeGAV(boolean nexus){
        return set(id, classifier, outputName,
                file, outputs,nexus,allClassifiers);
    }

    @Override
    public BaseArtifact call() throws ProcessingException {
        Delivery delivery = getRepository().chrono().getDelivery();
        BaseArtifact ret = new BaseArtifact();
        Artifact a;
        if (delivery.getArtifacts() != null && delivery.getArtifacts().containsKey(this.id)) {
            a = delivery.getArtifacts().get(this.id);
            if (a.getAttach() != null && this.classifier != null && a.getAttach().containsKey(this.classifier)){
                BaseArtifact attach = a.getAttach().get(this.classifier);
                if (attach.getOutputs() != null &&
                        attach.getOutputs().containsKey(this.outputName)){
                    attach.getOutputs().remove(this.outputName);
                }
                if (this.file) {
                    attach.setFile(null);
                }
                if (this.outputs) {
                    attach.setOutputs(null);
                }
                if (attach.equals(new BaseArtifact())){
                    a.getAttach().remove(this.classifier);
                    ret = null;
                } else {
                    a.getAttach().put(this.classifier,attach);
                    ret = attach;
                }
            } else {
                if (a.getOutputs() != null &&
                        a.getOutputs().containsKey(this.outputName)){
                    a.getOutputs().remove(this.outputName);
                }
                if (this.file) {
                    a.setFile(null);
                }
                if (this.outputs) {
                    a.setOutputs(null);
                }
                if (this.allClassifiers){
                    a.setAttach(null);
                }
                if (a.equals(new Artifact())){
                    delivery.getArtifacts().remove(this.id);
                    ret = null;
                } else {
                    delivery.getArtifacts().put(this.id,a);
                    ret = a;
                }
            }
        }
        getRepository().chrono().save(delivery);
        return ret;
    }
}
