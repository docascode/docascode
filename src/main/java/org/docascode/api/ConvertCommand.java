package org.docascode.api;

import org.apache.commons.io.FilenameUtils;
import org.docascode.api.core.converter.*;
import org.docascode.api.core.errors.DocAsCodeException;
import org.docascode.api.event.Event;

import java.io.File;

public class ConvertCommand extends DocAsCodeCommand<File> {
    private File fromFile;
    public ConvertCommand from(File fromFile){
        this.fromFile=fromFile;
        return this;
    }

    private File toFile;
    public ConvertCommand to(File toFile){
        this.toFile=toFile;
        return this;
    }

    @Override
    public File call() throws DocAsCodeException {
        AbstractConverter driver = new NotSupportedConversionDriver();
        String sourceExtension = FilenameUtils.getExtension(fromFile.getPath());
        String targetExtension = FilenameUtils.getExtension(toFile.getPath());
        Event converterEvent = new Event(this)
                .setMessage(String.format(
                        "No driver to convert %s to %s.",
                        fromFile,
                        toFile
                ))
                .setLevel(Event.Level.INFO);
        switch (sourceExtension) {
            case "docx":
                switch (targetExtension){
                    case "md":
                        converterEvent = new Event(this)
                                .setMessage(String.format(
                                        "Converting %s to %s.",
                                        fromFile,
                                        toFile
                                ));
                        driver = new PandocDriver();
                        break;
                    case "pdf":
                        converterEvent = new Event(this)
                                .setMessage(String.format(
                                        "Converting %s to %s.",
                                        fromFile,
                                        toFile
                                ));
                        driver = new DocxToPdfDriver();
                        break;
                    default:
                        break;
                }
                break;
            case "pptx":
                switch (targetExtension){
                    case "pdf":
                        converterEvent = new Event(this)
                                .setMessage(String.format(
                                        "Converting %s to %s.",
                                        fromFile,
                                        toFile
                                ));
                        driver = new PptxToPdfDriver();
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        fireEvent(converterEvent);
        driver.fromFile(fromFile)
                .toFile(toFile)
                .convert();
        return null;
    }
}
