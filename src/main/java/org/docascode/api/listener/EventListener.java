package org.docascode.api.listener;

import org.docascode.api.event.Event;

public interface EventListener {
    void fireEvent(Event event);
}
