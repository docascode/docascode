package org.docascode.api.event;

import java.util.EventObject;

public class Event extends EventObject {
    public enum Level {
        SUCESS,
        INFO,
        WARN,
        DEBUG;
    }

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public Event(Object source) {
        super(source);
    }

    private String message;

    public Event setMessage(String message){
        this.message = message;
        return this;
    }

    public String getMessage(){
        return this.message;
    }

    private Level level = Level.INFO;

    public Level getLevel(){
        return level;
    }

    public Event setLevel(Level level){
        this.level = level;
        return this;
    }
}
