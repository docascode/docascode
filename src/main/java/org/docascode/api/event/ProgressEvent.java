package org.docascode.api.event;

public class ProgressEvent extends Event {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ProgressEvent(Object source) {
        super(source);
    }

    private int progress = 0;

    private int total = 0;

    public ProgressEvent setProgress(int progress){
        this.progress = progress;
        return this;
    }

    public int getProgress(){
        return this.progress;
    }

    public ProgressEvent setTotal(int total){
        this.total = total;
        return this;
    }

    public int getTotal(){
        return this.total;
    }
}
