param (
    [string]$sBaseDoc,
    [string]$sMyDoc,
    [string]$sTheirDoc,
    [string]$sTargetDoc
)

Function mergeDoc-3Way($sBaseDoc, $sMyDoc, $sTheirDoc, $sTargetDoc)
{
$wdMergeTargetCurrent=1
$wdCompareDestinationOriginal=1
$wdGranularityWordLevel=1

$word = New-Object -ComObject word.application

$word.visible = $false;

$myDoc = $word.Documents.open($sBaseDoc);
#$myDoc.activate();
$myDoc.compare($sMyDoc,"mine", $wdMergeTargetCurrent, $true, $true);
$theirDoc = $word.Documents.Open($sTheirDoc);
try
{
    $doc = $word.MergeDocuments($theirDoc, $myDoc, $wdCompareDestinationOriginal, $wdGranularityWordLevel, $false, $true, $true, $true, $true, $true, $true, $true, $true, $true, "their", "mine");
    $doc.saveAs($sTargetDoc);
}
catch [system.exception]
{
    write-host "Failed to merge $sMyDoc and $sTheirDoc into $sTargetDoc...";
    exit 1
}
$word.quit();
}
mergeDoc-3Way $sBaseDoc $sMyDoc $sTheirDoc $sTargetDoc