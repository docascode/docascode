<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes" method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:template match="/">
<delivery xmlns="http://docascode.org/chrono/2.0">
	<artifacts>
		<xsl:for-each select="delivery/file">
			<artifact>
				<xsl:attribute name="id">
					<xsl:value-of select="@chrono" />
				</xsl:attribute>
				<file><xsl:value-of select="@input"/></file>
				<outputs>
					<xsl:if test="@output">
						<output name="default">
							<xsl:value-of select="@output"/>
						</output>
					</xsl:if>
					<xsl:for-each select="@*[name() != 'input' and name() != 'chrono' and name() != 'output']">
						<output name="{name()}">
							<xsl:value-of select="."/>
						</output>
					</xsl:for-each>
				</outputs>
				<xsl:if test="nexus">
					<maven>
						<groupId><xsl:value-of select="nexus/@groupID"/></groupId>
						<artifactId><xsl:value-of select="nexus/@artifactID"/></artifactId>
						<version><xsl:value-of select="nexus/@version"/></version>
					</maven>
				</xsl:if>
				<xsl:if test="attach">
					<attach>
						<xsl:for-each select="attach">
							<attach>
								<xsl:attribute name="classifier">
									<xsl:value-of select="@classifier" />
								</xsl:attribute>
								<file><xsl:value-of select="@input"/></file>
								<outputs>
									<xsl:if test="@output">
										<output name="default">
											<xsl:value-of select="@output"/>
										</output>
									</xsl:if>
									<xsl:for-each select="@*[name() != 'input' and name() != 'classifier' and name() != 'output']">
										<output name="{name()}">
											<xsl:value-of select="."/>
										</output>
									</xsl:for-each>
								</outputs>
							</attach>
						</xsl:for-each>
					</attach>
				</xsl:if>
			</artifact>
		</xsl:for-each>
	</artifacts>
</delivery>
</xsl:template>
</xsl:stylesheet>