#!/bin/bash

GIT_PROMPT_ONLY_IN_REPO=1
DOCASCODEDIR="$(dirname "$(dirname "$(which docascode)")")"

if [ -f "$DOCASCODEDIR/lib_ext/bash-git-prompt/gitprompt.sh" ]; then
  source "$DOCASCODEDIR/lib_ext/bash-git-prompt/gitprompt.sh"
fi
