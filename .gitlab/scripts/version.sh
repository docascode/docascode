#!/usr/bin/env bash
VERSION=$1
sed -i "s/#define AppVersion = .*/#define AppVersion = \"$1\";/g" "$( git rev-parse --show-toplevel )"/installer/windows/setup.iss

sed -i "s/version:.*/version: \'$1\'/g" "$( git rev-parse --show-toplevel )"/docs/antora.yml

mvn versions:set -DnewVersion=$1